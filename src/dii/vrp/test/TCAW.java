package dii.vrp.test;

import dii.vrp.data.*;
import dii.vrp.tp.*;

public class TCAW {

    public static void main(String[] args) {


        //Parameters
        String file= "./data/christofides-et-al-1979-cmt/CMT01.xml"; //Instance

        //Read data from an instance file
        System.out.println("Clarck and wright");
        IDistanceMatrix distances=null;
        IDemands demands=null;
        double Q=Double.NaN;
        try(VRPREPInstanceReader parser=new VRPREPInstanceReader(file)){
            distances=parser.getDistanceMatrix();
            demands=parser.getDemands();
            Q=parser.getCapacity("0");
            System.out.println(Q);
        }
        ClarkAndWright caw= new ClarkAndWright(distances,Q,demands,1);
        caw.run();



    }
}
