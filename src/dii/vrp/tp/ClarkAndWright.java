package dii.vrp.tp;

import dii.vrp.data.IDemands;
import dii.vrp.data.IDistanceMatrix;

import java.util.ArrayList;
import java.util.Collections;

public class ClarkAndWright implements IOptimizationAlgorithm {

    private ArrayList<Saving> savings;
    private IDistanceMatrix distances;
    private double capacity;
    private IDemands demandeNoeuds;
    private double lamda;
    /**
     * @param distances
     * @param capacity
     * @param demandeNoeuds
     */
    public ClarkAndWright(IDistanceMatrix distances, double capacity, IDemands demandeNoeuds,double lamda) {
        this.distances = distances;
        this.capacity = capacity;
        this.demandeNoeuds = demandeNoeuds;
        this.savings = new ArrayList<>();
        this.lamda=lamda;
    }

    /**
     * Calcul des savings
     */
    public void calculSaving() {

        for (int i = 1; i < distances.size(); i++) {
            for (int j = i; j < distances.size(); j++) {
                if (i != j) {
                    double calcul = distances.getDistance(0, i) + distances.getDistance(0, j) - lamda
                    *distances.getDistance(i, j);
                    Saving saving = new Saving(i, j, calcul);
                    this.savings.add(saving);
                }
            }
        }
        Collections.sort(savings);


    }

    @Override
    public ISolution run() {

        calculSaving();
        // n routes o i o
        VRPSolution vrpSolution= new VRPSolution();

        //Création de la solution de départ

        for(int i=1;i<distances.size();i++)
        {
            IRoute route= new VRPRoute();
            //Route 0 i 0 avec i>=1
            route.add(0);
            route.add(i);
            route.add(0);
            ((VRPRoute) route).setLoad(demandeNoeuds.getDemand(i));

            ((VRPRoute) route).setCost(distances.getDistance(0,i)+distances.getDistance(i,0));
            vrpSolution.addRoute(route);

        }
        //Affichage solution de départ
        //System.out.println(vrpSolution);

        //Parcours de la liste de savings
        for(Saving s:savings){
            //indice route dep
            int r=-1;
            //indice route arr
            int r_=-1;

            int posDep=0;
            int posEnd=0;

            // Obtenir les deux routes du savings
            for(int i=0;i<vrpSolution.size();i++)
            {
                IRoute route= vrpSolution.getRoute(i);
                int positionDep=route.positionOf(s.getIdDepart());
                int positionArr=route.positionOf(s.getIdArrivee());
                if(positionDep!=-1){
                    if(positionDep==1 || positionDep == route.size()-2)
                    {
                        if(i!=r_){
                            if(positionDep==route.size()-2){
                                posDep=0;
                            }
                            else
                            {
                                posDep=1;
                            }
                            r=i;

                        }
                    }
                }
                if(positionArr!=-1){
                    if(positionArr==1 || positionArr == route.size()-2)
                    {
                        if(i!=r){
                            if(positionArr==route.size()-2){
                                posEnd=0;
                            }
                            else
                            {
                                posEnd=1;
                            }
                            r_=i;

                        }

                    }
                }
            }


           if(r!=-1 && r_!=-1){

                   //La capacité du véhicule
                   IRoute route1=vrpSolution.getRoute(r);
                   IRoute route2=vrpSolution.getRoute(r_);
                   double demandeR=vrpSolution.getLoad(r);
                   double demandeR_=vrpSolution.getLoad(r_);


                   if(demandeR+demandeR_<=capacity){
                       IRoute route= new VRPRoute();

                       if(posDep==0 && posEnd==0){

                           for(int i=0;i<route1.size()-1;i++)
                           {
                               route.add(route1.get(i));

                           }
                           for(int i=route2.size()-2;i>=0;i--)
                           {

                               route.add(route2.get(i));

                           }
                           System.out.println(route.toString());
                       }
                       else if(posDep==0 && posEnd==1){
                           for(int i=0;i<route1.size()-1;i++)
                           {
                               route.add(route1.get(i));

                           }
                           for(int i=1;i<route2.size();i++)
                           {

                               route.add(route2.get(i));

                           }
                           System.out.println(route.toString());


                       }
                       else if(posDep==1 && posEnd==0){

                           for(int i=route1.size()-1;i>0;i--){
                               route.add(route1.get(i));
                           }
                           for(int i=route2.size()-2;i>=0;i--)
                           {
                               route.add(route2.get(i));
                           }

                           System.out.println(route.toString());



                       }
                       else if(posDep==1 && posEnd==1){

                           for(int i=route1.size()-1;i>0;i--)
                           {

                               route.add(route1.get(i));

                           }

                           for(int i=1;i<route2.size();i++)
                           {
                               route.add(route2.get(i));

                           }
                           System.out.println(route.toString());
                       }
                        

                       double costR=vrpSolution.getCost(r);
                       double costR_=vrpSolution.getCost(r_);

                       ((VRPRoute) route).setLoad(demandeR+demandeR_);
                       ((VRPRoute) route).setCost(costR+costR_-s.getSaving());


                       vrpSolution.remove(r);
                       if(r>r_){
                           vrpSolution.remove(r_);

                       }
                       else
                       {


                           vrpSolution.remove(r_-1);


                       }



                       vrpSolution.addRoute(route);

                   }
           }



        }
        vrpSolution.setOF(0);
        for(int i=0;i<vrpSolution.size();i++){
            vrpSolution.setOF(vrpSolution.getOF()+vrpSolution.getCost(i));

        }
        System.out.println(vrpSolution.getOF());

        System.out.println("Solution: "+vrpSolution);
        return vrpSolution;
    }


}
