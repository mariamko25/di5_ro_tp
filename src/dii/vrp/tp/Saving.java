package dii.vrp.tp;

public class Saving implements Comparable<Saving> {

    private  int idDepart;
    private  int idArrivee;

    private double saving;

    public Saving(int idDepart, int idArrivee, double saving) {
        this.idDepart = idDepart;
        this.idArrivee= idArrivee;
        this.saving = saving;
    }

    public int getIdDepart() {
        return idDepart;
    }

    public void setIdDepart(int idDepart) {
        this.idDepart = idDepart;
    }

    public int getIdArrivee() {
        return idArrivee;
    }

    public void setIdArrivee(int idArrivee) {
        this.idArrivee = idArrivee;
    }

    public double getSaving() {
        return saving;
    }

    public void setSaving(double saving) {
        this.saving = saving;
    }

    @Override
    public int compareTo(Saving o) {
        if(saving>o.saving)
            return -1;
        return this.saving==o.saving?0:1;
    }
}
